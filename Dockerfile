FROM node:14.17.3 as build
WORKDIR /app

COPY package.json /app
COPY yarn.lock /app
RUN yarn

COPY . .
RUN yarn build

CMD ["yarn", "start"]
