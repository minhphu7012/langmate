import Head from 'next/head';
import React from 'react';
import LandingPageLanguageMates from 'src/containers/LandingPageLanguageMates/index';

function Index() {
  return (
    <div className="language-landingpage">
      <Head>
        <title>Language Mates</title>
      </Head>

      <LandingPageLanguageMates />
    </div>
  );
}
export default Index;
