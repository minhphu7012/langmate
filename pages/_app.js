import App from 'next/app';
import React from 'react';
import { IntlProvider } from 'react-intl';
import { connect } from 'react-redux';
import { END } from 'redux-saga';
import { wrapper } from 'src/redux/store';
import 'styles/globals.css';
import 'styles/_app.scss';

class WrappedApp extends App {
  static async getInitialProps({ Component, ctx }) {
    // debugger;
    // 1. Wait for all page actions to dispatch
    // const pageProps = {
    //   ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {}),
    //   // ...(Component.getServerSideProps ? await Component.getServerSideProps(ctx) : {}),
    //   // ...(Component.getStaticProps ? await Component.getStaticProps(ctx) : {}),
    // };
    let pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    pageProps = {
      ...pageProps,
      ...(Component.getStaticProps ? await Component.getStaticProps(ctx) : {}),
    };
    pageProps = {
      ...pageProps,
      ...(Component.getServerSideProps ? await Component.getServerSideProps(ctx) : {}),
    };

    // 2. Stop the saga if on server
    if (ctx.req) {
      ctx.store?.dispatch(END);
      await ctx.store?.sagaTask.toPromise();
    }
    // 3. Return props
    return { pageProps };
  }

  render() {
    const { language } = this.props;
    const { Component, pageProps } = this.props;

    return (
      <IntlProvider>
          <Component {...pageProps} />
      </IntlProvider>
    );
  }
}

const mapState = (state) => {
  return { a: 1 };
};

export default wrapper.withRedux(connect(mapState)(WrappedApp));
