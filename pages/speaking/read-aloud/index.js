import Head from 'next/head';
import React from 'react';
import Layout from 'src/containers/Layout/Index';
import dynamic from 'next/dynamic';

const ReadAloud = dynamic(
  () => import('src/containers/Speaking/components/ReadAloud/Index'),
  {
    ssr: false,
  }
);

function Index() {
  return (
    <div className="">
      <Head>
        <title>LANGMATE LEARNER</title>
      </Head>

      <Layout>
        <ReadAloud />
      </Layout>
    </div>
  );
}
export default Index;
