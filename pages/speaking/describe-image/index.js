import Head from 'next/head';
import React from 'react';
import Layout from 'src/containers/Layout/Index';
import dynamic from 'next/dynamic';

const DescribeImage = dynamic(
  () => import('src/containers/Speaking/components/DescribeImage/Index'),
  {
    ssr: false,
  }
);

function Index() {
  return (
    <div className="">
      <Head>
        <title>LANGMATE LEARNER</title>
      </Head>

      <Layout>
        <DescribeImage />
      </Layout>
    </div>
  );
}
export default Index;
