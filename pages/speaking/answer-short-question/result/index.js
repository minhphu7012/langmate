import Head from 'next/head';
import React from 'react';
import Layout from 'src/containers/Layout/Index';
import dynamic from 'next/dynamic';

const AnswerShortQuestionResult = dynamic(
  () => import('src/containers/Speaking/components/AnswerShortQuestion/Result/Index'),
  {
    ssr: false,
  }
);

function Index() {
  return (
    <div className="">
      <Head>
        <title>LANGMATE LEARNER</title>
      </Head>

      <Layout type="result">
        <AnswerShortQuestionResult />
      </Layout>
    </div>
  );
}
export default Index;
