import Head from 'next/head';
import React from 'react';
import Layout from 'src/containers/Layout/Index';
import dynamic from 'next/dynamic';

const AnswerShortQuestion = dynamic(
  () => import('src/containers/Speaking/components/AnswerShortQuestion/Index'),
  {
    ssr: false,
  }
);

function Index() {
  return (
    <div className="">
      <Head>
        <title>LANGMATE LEARNER</title>
      </Head>

      <Layout>
        <AnswerShortQuestion />
      </Layout>
    </div>
  );
}
export default Index;
