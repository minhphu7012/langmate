import Head from 'next/head';
import React from 'react';
import Layout from 'src/containers/Layout/Index';
import dynamic from 'next/dynamic';

const Speaking = dynamic(() => import('src/containers/Speaking/Index'), {
  ssr: false,
});

function Index() {
  return (
    <div className="tiens-landingpage">
      <Head>
        <title>LANGMATE LEARNER</title>
      </Head>

      <Layout>
        <Speaking />
      </Layout>
    </div>
  );
}
export default Index;
