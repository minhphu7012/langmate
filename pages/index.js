import Head from 'next/head';
import React from 'react';
import Layout from 'src/containers/Layout/Index';
import dynamic from 'next/dynamic';
import { connect } from 'react-redux';
import Header from 'src/components/Header/Index';

const Homepage = dynamic(() => import('src/containers/Home/Index'), {
  ssr: false,
});

function Index() {
  return (
    <div className="tiens-landingpage">
      <Head>
        <title>LANGMATE LEARNER</title>
      </Head>
      <Header />
      <Homepage />
    </div>
  );
}

Index.getInitialProps = async ({ Component, ctx }) => {
  return {
    NEXT_PUBLIC_URL: process.env.NEXT_PUBLIC_URL,
  };
};

export default connect()(Index);
