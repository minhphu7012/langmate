import Head from 'next/head';
import React from 'react';
import Layout from 'src/containers/Layout/Index';
import dynamic from 'next/dynamic';

const Tips = dynamic(() => import('src/containers/Tips/Index'), {
  ssr: false,
});

function Index() {
  return (
    <div className="tiens-landingpage">
      <Head>
        <title>LANGMATE LEARNER</title>
      </Head>

      <Layout>
        <Tips />
      </Layout>
    </div>
  );
}
export default Index;
