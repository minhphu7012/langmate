

const nextConfig = {
  distDir: 'build',

  reactStrictMode: true,

  images: {
    domains: ['tiens-innoviera-api-test.aegona.click'],
  },
};
module.exports = nextConfig;
