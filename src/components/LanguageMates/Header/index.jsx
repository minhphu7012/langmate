import React, { useState } from 'react';
import Link from 'next/link';
import Logo from 'public/icons/ic-logo-language-mates.svg';
import IcToggle from 'public/icons/ic-landing-toggle-menu.png';
import IcClose from 'public/icons/ic-landing-close-menu.png';
import Image from 'next/image';
import { useEffect } from 'react';

function Index(props) {
  const [isOpen, setIsOpen] = useState();
  const [isSticky, setIsSticky] = useState(false);

  const checkSticky = () => {
    const scrollTop = window.scrollY;
    if (scrollTop > 0) {
      setIsSticky(true);
    } else {
      setIsSticky(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', checkSticky);
    return () => {
      window.removeEventListener('scroll', checkSticky);
    };
  }, []);

  return (
    <header
      className={`header grid grid-cols-5 sticky top-0 z-40  ${
        isSticky && 'active'
      }`}
    >
      <div className="flex items-center justify-center col-span-1">
        <Image src={Logo} alt="logo" />
      </div>
      <ul className="menu-list col-span-3 flex justify-center">
        <li className="menu-item">
          <Link href="/">PTE Practice</Link>
        </li>
        <li className="menu-item">
          <Link href="/">Mock Test</Link>
        </li>
        <li className="menu-item">
          <Link href="/">PTE Materials</Link>
        </li>
        <li className="menu-item">
          <Link href="/">Package</Link>
        </li>
        <li className="menu-item">
          <Link href="/">Blog</Link>
        </li>
        <li className="menu-item">
          <Link href="/">Contact us</Link>
        </li>
      </ul>
      <div className="actions col-span-1 flex items-center">
        <button className="btn--register">Register</button>
        <button className="btn--login">Login</button>
      </div>
      <div className="col-span-1 flex justify-end">
        <button className="btn-toggle" onClick={() => setIsOpen(true)}>
          <Image src={IcToggle} alt="Toggle" />
        </button>
      </div>
      <div className={`menu-mobile ${isOpen && 'open'}`}>
        <div className="menu-mobile__bg" />
        <div className="menu-mobile__content">
          <div className="flex justify-end">
            <button className="btn-close" onClick={() => setIsOpen(false)}>
              <Image src={IcClose} alt="Toggle" />
            </button>
          </div>
          <ul className="menu-list col-span-3 flex justify-center">
            <li className="menu-item">
              <Link href="/">PTE Practice</Link>
            </li>
            <li className="menu-item">
              <Link href="/">Mock Test</Link>
            </li>
            <li className="menu-item">
              <Link href="/">PTE Materials</Link>
            </li>
            <li className="menu-item">
              <Link href="/">Package</Link>
            </li>
            <li className="menu-item">
              <Link href="/">Blog</Link>
            </li>
            <li className="menu-item">
              <Link href="/">Contact us</Link>
            </li>
          </ul>
          <div className="actions col-span-1 flex items-center">
            <button className="btn--register">Register</button>
            <button className="btn--login">Login</button>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Index;
