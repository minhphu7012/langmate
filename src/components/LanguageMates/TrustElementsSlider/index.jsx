import React from 'react';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';
import Image from 'next/image';
import IcArrowLeft from 'public/icons/ic-landing-banner-arrow-left.svg';
import Banner1 from 'public/images/banner-landing-1.svg';
import Banner2 from 'public/images/banner-landing-2.svg';
import Banner3 from 'public/images/banner-landing-3.svg';

function Index() {
  var settings = {
    dots: true,
    infinite: true,
    dots: false,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: (
      <button>
        <Image src={IcArrowLeft} alt="" />
      </button>
    ),
    prevArrow: (
      <button>
        <Image src={IcArrowLeft} alt="" />
      </button>
    ),
  };
  return (
    <div className="slider">
      <Slider {...settings} className="slider__container">
        <div className="banner">
          <div className="banner__container flex flex-col items-center">
            <Image src={Banner1} alt="Trust Elements" />
            <h2>Test and get your score immediately.</h2>
          </div>
        </div>
        <div className="banner">
          <div className="banner__container flex flex-col items-center">
            <Image src={Banner2} alt="Trust Elements" />
            <h2>A Dashboard to keep track your performance</h2>
          </div>
        </div>
        <div className="banner">
          <div className="banner__container flex flex-col items-center">
            <Image src={Banner3} alt="Trust Elements" />
            <h2>Doing your test everywhere you want on mobile device</h2>
          </div>
        </div>
      </Slider>
      <div className="flex items-center justify-center">
        <button className="btn-register">Register Now</button>
      </div>
    </div>
  );
}

export default Index;
