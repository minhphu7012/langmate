import React from 'react';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';
import Image from 'next/image';
import IcArrowLeft from 'public/icons/ic-landing-banner-arrow-left.svg';
import ImgCustomer from 'public/images/img-landing-customer.svg';
import BgMess from 'public/images/bg-landing-customer-card.svg';

function Index() {
  var settings = {
    dots: true,
    infinite: true,
    speed: 700,
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: false,
    appendDots: (dots) => (
      <div>
        <ul> {dots} </ul>
      </div>
    ),
    responsive: [
      {
        breakpoint: 650,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };
  return (
    <div className="customer__container flex items-center">
      <div className="content">
        <p>Trusted by teams</p>
        <h3>75,000 people</h3>
        <p>
          Many companies look for designers who code and developers who design.
          They use our courses an example to train their new hires.
        </p>
      </div>
      {/* <div>slider</div>
      <div className="card">
        <div className="card__comment">
          <div className="bg" />
          <p>
            I love your system. Thank you for making it painless, pleasant and
            most of all hassle free! Thanks for the great service. Really good.
          </p>
        </div>
        <div className="card__info flex items-center">
          <div className="image">
            <Image src={ImgCustomer} alt="Customer" />
          </div>
          <div className="info">
            <p>Jane Cooper</p>
            <span>CEO, ABC Corporation</span>
          </div>
        </div>
      </div>{' '} */}
      <Slider {...settings} className="slider">
        <div className="card">
          <div className="card__comment">
            <div className="bg" />
            <p>
              I love your system. Thank you for making it painless, pleasant and
              most of all hassle free! Thanks for the great service. Really
              good.
            </p>
          </div>
          <div className="card__info flex items-center">
            <div className="image">
              <Image src={ImgCustomer} alt="Customer" />
            </div>
            <div className="info">
              <p>Jane Cooper</p>
              <span>CEO, ABC Corporation</span>
            </div>
          </div>
        </div>{' '}
        <div className="card">
          <div className="card__comment">
            <div className="bg" />
            <p>
              I love your system. Thank you for making it painless, pleasant and
              most of all hassle free! Thanks for the great service. Really
              good.
            </p>
          </div>
          <div className="card__info flex items-center">
            <div className="image">
              <Image src={ImgCustomer} alt="Customer" />
            </div>
            <div className="info">
              <p>Jane Cooper</p>
              <span>CEO, ABC Corporation</span>
            </div>
          </div>
        </div>{' '}
        <div className="card">
          <div className="card__comment">
            <div className="bg" />
            <p>
              I love your system. Thank you for making it painless, pleasant and
              most of all hassle free! Thanks for the great service. Really
              good.
            </p>
          </div>
          <div className="card__info flex items-center">
            <div className="image">
              <Image src={ImgCustomer} alt="Customer" />
            </div>
            <div className="info">
              <p>Jane Cooper</p>
              <span>CEO, ABC Corporation</span>
            </div>
          </div>
        </div>{' '}
      </Slider>
    </div>
  );
}

export default Index;
