import React, { useState, useEffect } from 'react';
import ClockIcon from 'public/icons/ic-clock.svg';
import IconRedDot from 'public/icons/ic-red-dot.svg';
import IconCheck from 'public/icons/ic-check.svg';
import IconSpeaker from 'public/icons/ic-speaker.svg';
import IconHeadPhone from 'public/icons/ic-headphone.svg';
import Image from 'next/image';
import { Statistic, Progress, Slider } from 'antd';
import { useRouter } from 'next/router';

const { Countdown } = Statistic;
const timeSpeak = 40000;

function Index({ title, isListen, data }) {
  const [status, setStatus] = useState('countdown');
  const [percentProgress, setPercentProgress] = useState(0);
  const [timeCoundown, setTimeCountDown] = useState(4);
  const router = useRouter();
  useEffect(() => {
    // setTimeCountDown(4);
    setStatus('countdown');
    // setPercentProgress(0);
  }, [data]);

  const onChange = (val) => {
    if (4.95 * 1000 < val && val < 5 * 1000) {
      console.log('changed!');
    }
  };

  const handleCountDownFinish = () => {
    setStatus('recording');
    setInterval(() => {
      setPercentProgress((pre) => pre + 2.5);
    }, 1000);
    setTimeout(() => {
      setStatus('done');
    }, timeSpeak);
  };

  useEffect(() => {
    if (status === 'done') {
      setTimeout(() => {
        router.push(`${router.pathname}/result`);
      }, 1000);
    }
  }, [status]);

  const onChangeSlider = (value) => {
    console.log('onChange: ', value);
  };

  const onAfterChangeSlider = (value) => {
    console.log('onAfterChange: ', value);
  };

  return (
    <div className="lm-progress w-3/5 mb-[32px]">
      <div className="lm-progress__title flex justify-between items-center">
        <p className="font-bold">{title}</p>
        {isListen ? (
          <div className="flex">
            <Image src={IconHeadPhone} alt="clocl-icon" />
            <div className="ml-1 text-red-100 font-semibold">Listening</div>
          </div>
        ) : status === 'countdown' ? (
          <div className="flex">
            <Image src={ClockIcon} alt="clocl-icon" />
            <div className="ml-1">Beginning in</div>
            <div>
              <Countdown
                format="ss"
                onFinish={handleCountDownFinish}
                value={Date.now() + timeCoundown * 1000}
                onChange={onChange}
              />
            </div>
            seconds
          </div>
        ) : status === 'recording' ? (
          <div className="flex">
            <Image src={IconRedDot} alt="clocl-icon" />
            <div className="ml-1 text-red-100 font-semibold">Recording</div>
          </div>
        ) : (
          <div className="flex">
            <Image src={IconCheck} alt="clocl-icon" />
            <div className="ml-1 text-green-400 font-semibold">Done</div>
          </div>
        )}
      </div>
      <div
        className={`w-[96%] h-[10px] mx-auto ${
          title === 'Audio' ? 'mt-2' : 'mt-6'
        }`}
      >
        {title === 'Audio' ? (
          <div className="flex mb-1">
            <Image src={IconSpeaker} alt="clocl-icon" />
            <div className="w-full ml-2">
              <Slider
                defaultValue={50}
                onChange={onChangeSlider}
                onAfterChange={onAfterChangeSlider}
              />
            </div>
          </div>
        ) : (
          ''
        )}
        <Progress percent={percentProgress} showInfo={false} />
      </div>
    </div>
  );
}

export default Index;
