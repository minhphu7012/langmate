import Image from 'next/image';
import LmLogo from 'public/icons/ic-logo-header.svg';
import React from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Dropdown, Menu, Space } from 'antd';

function Index() {
  const router = useRouter();
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <Link href="/speaking/read-aloud">
              <a>Read Aloud</a>
            </Link>
          ),
        },

        {
          key: '2',
          label: (
            <Link href="/speaking/describe-image">
              <a>Describe Image</a>
            </Link>
          ),
        },
        {
          key: '3',
          label: (
            <Link href="/speaking/answer-short-question">
              <a>Answer Short Question</a>
            </Link>
          ),
        },
      ]}
    />
  );
  return (
    <div className="lm-header flex items-center justify-between">
      <div className="lm-header__logo">
        <Link href="/">
          <a>
            <Image
              className="tiens-logo"
              src={LmLogo}
              layout="intrinsic"
              alt="lm-logo"
            />
          </a>
        </Link>
      </div>
      <div className="lm-header__nav">
        <Menu mode="horizontal">
          <Menu.Item key="Speaking">
            <Dropdown overlay={menu}>
              <a onClick={(e) => e.preventDefault()}>
                <Link href="/speaking">
                  <a>Speaking</a>
                </Link>
              </a>
            </Dropdown>
          </Menu.Item>
          <Menu.Item key="Writing">
            <Link href="/writing">
              <a>Writing</a>
            </Link>
          </Menu.Item>
          <Menu.Item key="Reading">
            <Link href="/reading">
              <a>Reading</a>
            </Link>
          </Menu.Item>
          <Menu.Item key="Listening">
            <Link href="/listening">
              <a>Listening</a>
            </Link>
          </Menu.Item>
        </Menu>
      </div>
    </div>
  );
}

export default Index;
