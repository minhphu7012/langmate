import React, { useState } from 'react';
import { Statistic } from 'antd';
const { Countdown } = Statistic;

const timeCoundown = 2;

function Index() {
  const [status, setStatus] = useState(['countdown']);

  const onChange = (val) => {
    if (4.95 * 1000 < val && val < 5 * 1000) {
      console.log('changed!');
    }
  };

  const handleCountDownFinish = () => {
    setStatus('recording');
  };

  return (
    <Countdown
      format="ss"
      onFinish={handleCountDownFinish}
      value={Date.now() + timeCoundown * 1000}
      onChange={onChange}
    />
  );
}

export default Index;
