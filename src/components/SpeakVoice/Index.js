import React, { useState, useEffect } from 'react';
import { Button } from 'antd';
import SpeechRecognition, {
  useSpeechRecognition,
} from 'react-speech-recognition';

function SpeakerVoice(props) {
  const { setVoice } = props;

  const { transcript, finalTranscript, resetTranscript, getRecognition } =
    useSpeechRecognition();
  const [listening, setListening] = useState(false);

  useEffect(() => {
    // resetTranscript();
    setVoice(transcript);
  }, [transcript]);

  useEffect(() => {
    setTimeout(() => {
      _onStartSpeakerVoice();
    }, 3000);
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    alert(e.target.text.value);
  };

  function _onStartSpeakerVoice() {
    // resetTranscript();
    setListening(true);
    SpeechRecognition.startListening({ continuous: true, language: 'en-GB' });
  }

  function _onStopSpeakerVoice() {
    setListening(false);
    setVoice(transcript);

    SpeechRecognition.stopListening();
  }

  if (!SpeechRecognition.browserSupportsSpeechRecognition()) {
    return null;
  }

  return (
    <React.Fragment>
      {/* {!listening ? (
        <Button className="btn-micro" onClick={_onStartSpeakerVoice}>
          play
        </Button>
      ) : (
        <Button className="btn-micro" onClick={_onStopSpeakerVoice}>
          pause
        </Button>
      )}
      <div>
        <form onSubmit={handleSubmit}>
          <textarea
            name="text"
            rows={10}
            cols={50}
            value={transcript}
          ></textarea>
          <div className="btn-container">
            <span onClick={resetTranscript} className="btn">
              Clear Text
            </span>
            <button type="submit" className="btn">
              Print Text
            </button>
          </div>
        </form>
      </div> */}
    </React.Fragment>
  );
}

export default SpeakerVoice;
