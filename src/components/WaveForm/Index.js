import React, { useEffect, useRef, useState } from 'react';
import WaveSurfer from 'wavesurfer.js';
import PlayIcon from '/public/icons/ic-play.svg';
import PauseIcon from '/public/icons/ic-pause.svg';
import Image from 'next/image';
import IconSpeaker from 'public/icons/ic-speaker.svg';
import { Select } from 'antd';

const { Option } = Select;

export default function Waveform({}) {
  const waveformRef = useRef(null);
  const wavesurfer = useRef(null);
  const [playing, setPlay] = useState(false);
  const [speedAudio, setSpeedAudio] = useState(1);
  const [volume, setVolume] = useState(0.5);

  const formWaveSurferOptions = (ref) => ({
    container: ref,
    audioRate: speedAudio,
    waveColor: '#A9A9A9',
    progressColor: '#53A4BA',
    cursorColor: '#53A4BA',
    barWidth: 3,
    barRadius: 3,
    responsive: true,
    height: 100,
    normalize: true,
    partialRender: true,
  });

  const url =
    'https://www.mfiles.co.uk/mp3-downloads/brahms-st-anthony-chorale-theme-two-pianos.mp3';

  const url1 =
    'http://localhost:3000/08d44044-0b40-47de-9bf6-b14e51d1b22d';
  // create new WaveSurfer instance
  // On component mount and when url changes
  useEffect(() => {
    setPlay(false);
    const options = formWaveSurferOptions(waveformRef.current);
    wavesurfer.current = WaveSurfer.create(options);
    wavesurfer?.current?.load(url);
    wavesurfer.current.on('ready', function () {
      // https://wavesurfer-js.org/docs/methods.html
      // wavesurfer.current.play();
      // setPlay(true);

      // make sure object stillavailable when file loaded
      if (wavesurfer.current) {
        wavesurfer.current.setVolume(volume);
        setVolume(volume);
      }
    });

    // Removes events, elements and disconnects Web Audio nodes.
    // when component unmount
    return () => wavesurfer.current.destroy();
  }, []);

  const handlePlayPause = () => {
    setPlay(!playing);
    wavesurfer.current.playPause();
  };

  const onVolumeChange = (e) => {
    const { target } = e;
    const newVolume = +target.value;

    if (newVolume) {
      setVolume(newVolume);
      wavesurfer.current.setVolume(newVolume || 1);
    }
  };

  const handleChange = (value) => {
    setSpeedAudio(value);
  };
  return (
    <div className="flex items-center justify-between gap-2">
      <div className="" onClick={handlePlayPause}>
        {!playing ? (
          <div className="play-btn">
            <Image src={PlayIcon} alt="play-icon" />
          </div>
        ) : (
          <div className="pause-btn">
            <Image src={PauseIcon} alt="play-icon" />
          </div>
        )}
      </div>

      <div className="w-[70%]">
        <div id="waveform" ref={waveformRef} />
      </div>

      <div className="">
        <div className="flex items-center gap-1">
          <Image src={IconSpeaker} alt="play-icon" />
          <input
            className="slider"
            type="range"
            id="volume"
            name="volume"
            min="0.01"
            max="1"
            step=".025"
            onChange={onVolumeChange}
            defaultValue={volume}
          />
        </div>

        <div className="lm-footer__select speed mt-2 flex items-center gap-2">
          <span>Speed</span>
          <Select
            defaultValue="0.5x"
            dropdownMatchSelectWidth={false}
            placement="bottomLeft"
            onChange={handleChange}
          >
            <Option value={0.5}>0.5x</Option>
            <Option value={1}>1x</Option>
            <Option value={2}>2x</Option>
          </Select>
        </div>
      </div>
    </div>
  );
}
