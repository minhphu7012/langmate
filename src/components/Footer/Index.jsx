import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Image from 'next/image';
import { Select } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import QuestionIcon from '/public/icons/ic-question.svg';
import BookmarkIcon from '/public/icons/bookmark.svg';
import PreIcon from '/public/icons/ic-pre.svg';
import NextIcon from '/public/icons/ic-next.svg';
import speakingActions from 'src/redux/speaking/actions';

const { getReadAloud } = speakingActions;

const { Option } = Select;

function Index({ type }) {
  const router = useRouter();

  const dispatch = useDispatch();
  const { dataReadAloud, listQuestion } = useSelector(
    (state) => state.Speaking
  );

  useEffect(() => {
    setValueSelect(`Question ${dataReadAloud.questionNumber}`);
  }, [dataReadAloud]);

  // console.log(dataReadAloud);
  const [valueSelect, setValueSelect] = useState('Question 1');

  const handleChange = (value, option) => {
    setValueSelect(option.children);
    dispatch(
      getReadAloud({
        Question: Number(option.key),
        Id: value,
      })
    );
  };

  const handleClickNextQuestion = () => {
    dispatch(
      getReadAloud({
        Question: dataReadAloud.questionNumber + 1,
        Id: dataReadAloud?.listQuestion[dataReadAloud.questionNumber]?.id,
      })
    );
  };

  const handleClickPreQuestion = () => {
    dispatch(
      getReadAloud({
        Question: dataReadAloud.questionNumber - 1,
        Id: dataReadAloud?.listQuestion[dataReadAloud.questionNumber - 2]?.id,
      })
    );
  };

  return (
    <div className="lm-footer absolute bottom-0 flex justify-between items-center">
      <div className="flex w-28	 justify-between">
        <Image src={QuestionIcon} alt="question-icon" />
        <Image src={BookmarkIcon} alt="question-icon" />
      </div>

      {type === 'result' ? (
        <div className="lm-footer-btn try-again-btn">
          <Link href={router.pathname.slice(0, -7)}>
            <a className="w-full h-full flex items-center justify-center">
              TRY AGAIN
            </a>
          </Link>
        </div>
      ) : (
        <div className="flex">
          <div className="lm-footer-btn check-btn mr-[12px] ">
            <Link href={`${router.pathname}/result`}>
              <a className="w-full h-full flex items-center justify-center hover:text-red">
                CHECK
              </a>
            </Link>
          </div>
          <div className="lm-footer-btn solution-btn">
            <Link href="">
              <a className="w-full h-full flex items-center justify-center">
                SOLUTION
              </a>
            </Link>
          </div>
        </div>
      )}

      <div className="flex items-center	justify-between gap-2	">
        <div onClick={handleClickPreQuestion} className="pre-btn lm-footer-btn">
          <Image src={PreIcon} alt="question-icon" />
          <span>PREV</span>
        </div>
        <div className="lm-footer__select">
          <Select
            onChange={handleChange}
            defaultValue="Question 1"
            style={{
              width: 120,
            }}
            value={valueSelect}
            dropdownMatchSelectWidth={false}
            placement="topLeft"
          >
            {listQuestion.length > 0
              ? listQuestion.map((ques, index) => {
                  return (
                    <Option value={ques.id} key={index + 1}>{`Question ${
                      index + 1
                    }`}</Option>
                  );
                })
              : []}
          </Select>
        </div>

        <div
          onClick={handleClickNextQuestion}
          className="next-btn lm-footer-btn"
        >
          <span>NEXT</span>
          <Image src={NextIcon} alt="question-icon" />
        </div>
      </div>
    </div>
  );
}

export default Index;
