import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { Select } from 'antd';
const { Option } = Select;
import { PieChart } from 'react-minimal-pie-chart';

function Index() {
  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };

  return (
    <div className="lm-homepage">
      <p className="lm-homepage__progress">
        You’ve practised 21 / 510 questions
      </p>

      <div className="w-3/5 mx-auto">
        <PieChart
          animation
          animationDuration={500}
          animationEasing="ease-out"
          center={[50, 50]}
          data={[
            {
              color: '#DCDCDC',
              title: 'One',
              value: 10,
            },
            {
              color: '#69B4F1',
              title: 'Two',
              value: 15,
            },
          ]}
          labelPosition={50}
          lengthAngle={360}
          lineWidth={15}
          paddingAngle={0}
          radius={50}
          rounded
          startAngle={0}
          viewBoxSize={[100, 100]}
        />
      </div>

      <p className="lm-homepage__title mt-4">DO YOU WISH TO</p>
      <div className="lm-homepage__item">
        <Link href="/">
          <a>CONTINUE</a>
        </Link>
      </div>
      <div className="lm-homepage__item">
        <Link href="/">
          <a>START OVER</a>
        </Link>
      </div>
      <div className="lm-homepage__item">
        <Link href="/">
          <a>BOOKMARK</a>
        </Link>
      </div>
      <div className="lm-homepage__item lm-homepage__select">
        <Select
          defaultValue="QUESTION 234"
          style={{
            width: 150,
          }}
          onChange={handleChange}
        >
          <Option value="jack">QUESTION 235</Option>
          <Option value="lucy">QUESTION 236</Option>
        </Select>
      </div>
    </div>
  );
}

export default Index;
