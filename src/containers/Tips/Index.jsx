import Image from 'next/image';
// import ArrowIcon from 'public/icons/ic-white-arrow-down.svg';
import React, { useState } from 'react';
import { Tabs } from 'antd';

const { TabPane } = Tabs;
function Index() {
  return (
    <div className="lm-tips">
      <p>
        MY SCORE: <span>83%</span>
      </p>
      <div>
        <Tabs defaultActiveKey="1">
          <TabPane tab="Your Response" key="1">
            Tab 1
          </TabPane>
          <TabPane tab="Sample Solution" key="2">
            Tab 2
          </TabPane>
          <TabPane tab="Tips" key="3">
            <p>
              Pariatur libero quas possimus quis similique tenetur voluptate
              consectetur sunt. Quibusdam optio corporis sunt unde. Est sed
              tenetur est alias eveniet voluptas. Aut inventore consectetur
              similique nesciunt repellendus sint eaque repudiandae explicabo.
              Repellendus ducimus omnis quod quasi amet id repellendus
              voluptatem. Tempore iure tempora.
            </p>
            <iframe
              width="100%"
              height="100%"
              src="https://www.youtube-nocookie.com/embed/q8GxYDrtuEw"
              title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            ></iframe>
            <p>
              Cumque aliquid dicta. Quidem qui et. Inventore inventore officiis
              tenetur cupiditate pariatur beatae atque earum. Id quis sunt autem
              velit ullam earum dolorem accusantium. Inventore molestiae minus
              voluptatem ducimus quidem ratione quos dolor atque. Nesciunt et
              laboriosam hic ut sit aut quasi. Assumenda reprehenderit harum
              quis voluptatem voluptas. Magnam suscipit occaecati nihil nihil et
              ut. Esse eos sed. Sint sint molestiae enim hic et enim.
              Perspiciatis earum facilis laboriosam in earum a. Sunt
              consequuntur quia laudantium voluptas. Reiciendis ullam
              voluptatem. Esse voluptatem consequuntur beatae. Quaerat saepe
              neque qui ut consectetur voluptatem repellendus. Rem et optio enim
              temporibus nihil consequatur consectetur cumque.
            </p>
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
}

export default Index;
