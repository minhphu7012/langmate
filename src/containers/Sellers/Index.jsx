import Image from 'next/image';
import SellersBannerImage from 'public/images/img-banner-sellers.jpg';
import YouKnowOneImage from 'public/images/img-do-you-know-1.jpg';
import YouKnowImage from 'public/images/img-do-you-know.jpg';
import YouKnowTwoImage from 'public/images/img-do-you-want.png';
import JoinWithUsImage from 'public/images/img-join-with-us.jpg';
import PhoneImage from 'public/images/img-phone.png';
import React, { useState } from 'react';
import BannerTop from 'src/components/Banner/BannerTop';
import SellersFeedbacks from 'src/components/Feedbacks/SellersFeedbacks';
import ReasonToJoin from 'src/components/ReasonToJoin/Index';
import Training from 'src/components/Training/Index';
import Verified from 'src/components/Verified/Index';
import Achievement from 'src/components/Achievement/Index';
import Benefits from 'src/components/Benefits/Index';
import ModalContact from 'src/components/ModalContact/Index';

function Index() {
  const [visible, setVisible] = useState(false);

  const DO_YOU_KNOW = [
    {
      key: 6,
      name: 'Bán hàng trực tuyến đạt doanh thu 13,7 tỷ đô năm 2021 và bây giờ là năm 2022',
      color: 'white-500',
    },
    {
      key: 5,
      name: 'Mỗi tháng Shopee nhận 77,8 triệu lượt truy cập',
      color: 'beigeE9',
    },
    {
      key: 4,
      name: 'Việt Nam có 49,3 triệu người tham gia mua sắm trực tuyến',
      color: 'orangeDarkFF',
    },
    {
      key: 3,
      name: '52% Người dùng trực tuyến mua các sản phẩm sức khỏe',
      color: 'white-500',
    },
    {
      key: 2,
      name: 'Nhiều người bán bán hàng thông qua Quảng cáo Facebook',
      color: 'beigeE9',
    },
    {
      key: 1,
      name: '11 triệu đơn hàng vào Shopee trong 24 giờ',
      color: 'orangeDarkFF',
    },
  ];
  return (
    <div className="tiens-sellers">
      <BannerTop bannerUrl={SellersBannerImage}>
        <div className="tiens-banner__introduce w-full absolute xs:px-23px sm:px-45px lg:px-70px z-20">
          <div className="sm:w-128 flex flex-col items-start justify-center sm:text-5.5xl xs:text-1xl font-semibold">
            {/* <h1 className="text-orangeDarkFF">Chương trình</h1> */}
            <h1 className="text-orangeDarkFF xl:text-6.5xl lg:text-4xl xs:text-3xl lg:mb-6 xs:mb-2">
              Người bán hàng
            </h1>
            <ul className="text-white-500 font-semibold lg:text-3xl xs:text-xl">
              <li className="mb-2">Bán hàng là có tiền</li>
              <li className="mb-2">Bán hàng là phục vụ</li>
              <li className="">Bán hàng là làm giàu</li>
            </ul>
            {/* <h5 className="text-white-500 font-semibold lg:text-2.2xl xs:text-xl">
              Đã đến lúc bạn tham gia cùng hàng nghìn người khác trong Chương
              trình người bán và kiếm{' '}
              <span className="text-orangeDarkFF">hàng triệu đồng</span> ngay
              tại nhà
            </h5> */}
          </div>
        </div>
      </BannerTop>

      <div className="mt-negative">
        <div className="xs:px-23px sm:px-45px lg:px-70px xs:py-6 sm:py-12 xl:py-18 bg-black18">
          <div className="grid lg:grid-cols-2 xs:grid-cols-1 lg:gap-x-12 xs:gap-y-12 items-center">
            <div className="w-full xl:h-128 sm:h-96 xs:h-80">
              <iframe
                width="100%"
                height="100%"
                src="https://www.youtube.com/embed/q8GxYDrtuEw"
                title="YouTube video player"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              ></iframe>
            </div>

            <div className="text-center">
              <h1 className="text-orangeDarkFF lg:text-4xl xs:text-3xl font-black capitalize mb-6">
                Thời đại công nghệ số
              </h1>
              <p className="text-white-500 sm:text-2.2xl xs:text-1xl font-semibold">
                Thời thế đã thay đổi, cách kiếm tiền cũng đã thay đổi, bạn đã
                tham gia bán hàng trực tiếp online chưa? Bạn có biết những gì
                đang diễn ra ngay bây giờ trong thế giới công nghệ số không?
              </p>
            </div>
          </div>
        </div>
      </div>

      <div className="xs:px-23px sm:px-45px lg:px-70px xs:py-6 sm:py-12 xl:py-18 h-auto">
        <div className="grid lg:grid-cols-2 xs:grid-cols-1 lg:gap-x-12 lg:gap-y-0 xs:gap-y-8 items-center">
          <div className="relative rounded-md overflow-hidden">
            <div className="-mb-2">
              <Image
                src={YouKnowImage}
                layout="intrinsic"
                alt="sellers-image"
              />
            </div>
            <div className="absolute top-0 w-full h-full bg-lightestBlackRBGA flex flex-row items-center justify-center">
              <h1 className="lg:text-4xl xs:text-3xl text-white-500 font-black">
                Bạn có biết
              </h1>
            </div>
          </div>

          <ul>
            {DO_YOU_KNOW.map((item, index) => (
              <li
                key={item.key}
                style={{
                  transform: `translateY(${-index * 18}px)`,
                }}
                className={`w-full relative z-${
                  item.key * 10
                } text-1xl text-center text-black-900 font-semibold pt-11 pb-6 px-4 rounded-br-2.5xl rounded-bl-2.5xl bg-${
                  item.color
                }`}
              >
                {item.name}
              </li>
            ))}
          </ul>
        </div>
      </div>

      <div>
        <div className="flex flex-row lg:flex-nowrap xs:flex-wrap items-stretch">
          <div className="lg:w-7/12 xs:w-full bg-black18 xs:px-23px sm:px-45px lg:px-70px xs:py-6 sm:py-12 xl:py-18">
            <h1 className="lg:text-4xl xs:text-3xl text-center text-orangeDarkFF font-black mb-6">
              Còn bạn thì sao ?
            </h1>
            <p className="text-center text-white-500 lg:text-2.2xl xs:text-1xl font-semibold mb-6">
              Bạn đã theo dõi tất cả dữ liệu ở trên với tư cách là người mua
              chưa?
            </p>
            <p className="text-center text-white-500 lg:text-2.2xl xs:text-1xl font-semibold">
              Bây giờ, đã đến lúc bạn tham gia vào các{' '}
              <span className="text-orangeDarkFF">dữ liệu khổng lồ</span> trên
              bằng cách tham gia với tư cách Người bán và có cơ hội nhận{' '}
              <span className="text-orangeDarkFF">hàng triệu đồng</span> ngay
              tại nhà
            </p>
          </div>
          <div className="lg:w-5/12 xs:w-full">
            <div className="-mb-2">
              <Image
                className="w-full h-full object-cover"
                src={YouKnowOneImage}
                layout="intrinsic"
                alt="sellers-image"
              />
            </div>
          </div>
        </div>
      </div>

      <div className="mt-negative">
        <div className="grid lg:grid-cols-2 xs:grid-cols-1 items-center xs:px-23px sm:px-45px lg:px-70px xs:py-6 sm:py-12 xl:py-18 bg-black18 lg:gap-x-12 lg:gap-y-0 xs:gap-y-8">
          <div className="max-w-md">
            <h1 className="text-white-500 lg:text-5.5xl xs:text-3xl font-semibold text-center">
              Bạn muốn có ?
            </h1>
            <div className="-mb-2">
              <Image
                src={YouKnowTwoImage}
                layout="intrinsic"
                alt="sellers-image"
              />
            </div>
          </div>

          <ul className="">
            <li className="text-white-500 lg:text-3.5xl xs:text-2xl font-semibold mb-6 flex flex-row items-start">
              <span className="flex-shrink-0 text-gray80 lg:text-4xl xs:text-2.5xl mr-5">
                01
              </span>
              <h2 className="text-white-500">
                Bạn muốn có thêm{' '}
                <span className="text-orangeDarkFF">thu nhập?</span>
              </h2>
            </li>

            <li className="text-white-500 lg:text-3.5xl xs:text-2xl font-semibold mb-6 flex flex-row items-start">
              <span className="text-gray80 lg:text-4xl xs:text-2.5xl mr-5">
                02
              </span>
              <h2 className="text-white-500">
                <span className="text-orangeDarkFF">Bạn muốn khởi nghiệp</span>,
                nhưng ít vốn?
              </h2>
            </li>

            <li className="text-white-500 lg:text-3.5xl xs:text-2xl font-semibold mb-6 flex flex-row items-start">
              <span className="text-gray80 lg:text-4xl xs:text-2.5xl mr-5">
                03
              </span>
              <h2 className="text-white-500">
                Bạn
                <span className="text-orangeDarkFF">muốn bắt đầu</span>, nhưng
                bối rối không biết sản phẩm nào là sản phẩm phù hợp để bán?
              </h2>
            </li>

            <li className="text-white-500 lg:text-3.5xl xs:text-2xl font-semibold flex flex-row items-start">
              <span className="text-gray80 lg:text-3.5xl xs:text-2xl mr-5">
                04
              </span>
              <h2 className="text-white-500">
                <span className="text-orangeDarkFF">Muốn bán hàng</span> nhưng
                thiếu kinh nghiệm, không biết quảng cáo ??
              </h2>
            </li>
          </ul>
        </div>
      </div>

      <div>
        <div className="grid lg:grid-cols-2 xs:grid-cols-1 items-center xs:px-23px sm:px-45px lg:px-70px xs:py-6 sm:py-12 xl:py-18 bg-black18 font-semibold text-center lg:text-3.5xl xs:text-2xl">
          <div className="">
            <h2 className="text-orangeDarkFF lg:mb-8 xs:mb-4 text-3.5xl">
              Bình tĩnh,
            </h2>
            <p className="text-white-500">
              Chúng tôi với hàng chục năm kinh nghiệm xây dựng hệ thống bán hàng
            </p>
          </div>

          <div className="">
            <p className="text-white-500 lg:mb-8 xs:mb-4">
              {`Chúng tôi sẽ hỗ trợ bạn thực hiện và giúp bạn tạo thu nhập tại
              nhà, với nguồn vốn INTERNET & Social Media có thể thực hiện mọi
              lúc mọi nơi!`}
            </p>
            <h2 className="text-orangeDarkFF">Chúng tôi có giải pháp</h2>
          </div>
        </div>
      </div>

      <div>
        <ReasonToJoin />
      </div>

      <div className="">
        <div className="xs:px-23px sm:px-45px lg:px-70px xs:py-6 sm:py-12 xl:py-18 bg-black18 flex flex-row sm:flex-nowrap xs:flex-wrap lg:gap-x-18 sm:gap-x-10 sm:gap-y-0 xs:gap-y-9 items-start">
          <div className="sm:w-1/2 xs:w-full">
            <h1 className="text-white-500 lg:text-5.5xl xs:text-3xl font-semibold mb-8 text-center">
              {`Let ' s work together`}
            </h1>
            <div className="-mb-2">
              <Image
                src={JoinWithUsImage}
                layout="intrinsic"
                alt="join-with-us-image"
              />
            </div>
          </div>

          <div className="sm:w-1/2 xs:w-full text-center">
            <div className="lg:mb-8 xs:mb-4 max-w-xs mx-auto">
              <Image
                src={PhoneImage}
                layout="intrinsic"
                alt="join-with-us-image"
              />
            </div>
            <button
              className="bg-orangeDarkFF lg:text-3.5xl xs:text-2.2xl text-black-900 font-semibold rounded-10px p-4 lg:hover:opacity-80"
              onClick={() => setVisible(true)}
            >
              Tham gia ngay
            </button>
          </div>
        </div>
      </div>

      <div>
        <SellersFeedbacks />
      </div>

      <div>
        <Training />
      </div>

      <div>
        <Verified />
      </div>

      <div>
        <Achievement />
      </div>

      <div>
        <Benefits />
      </div>

      <ModalContact visible={visible} setVisible={() => setVisible(false)} />
    </div>
  );
}

export default Index;
