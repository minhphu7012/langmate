import React from 'react';
import Footer from 'src/components/Footer/Index';
import Header from 'src/components/Header/Index';

function Index({ children, type }) {
  return (
    <div className="h-screen relative z-50">
      <Header />
      <div className="px-[70px]">{children}</div>
      <Footer type={type} />
    </div>
  );
}

export default Index;
