import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { Select } from 'antd';
import RecordedAnswer from 'src/components/RecordedAnswer/Index';

function Index() {
  return (
    <div className="lm-read-aloud mt-[32px]">
      <div className="flex flex-col items-center">
        <p className="font-bold mb-[32px]">
          You will hear a question. Please give a simple and short answer. Often
          just once or a few words is enough.
        </p>
        <RecordedAnswer title="Audio" />

        <RecordedAnswer title="Recorded Answer" isListen={true} />
      </div>
    </div>
  );
}

export default Index;
