import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { Tabs } from 'antd';
const { TabPane } = Tabs;
import Waveform from '../../../../../components/WaveForm/Index';

function Index() {
  const onChange = (key) => {
    console.log(key);
  };

  return (
    <div className="lm-speak-aloud-result">
      <p className="result__title text-2xl text-center font-bold py-[32px]">
        MY SCORE: <span className="text-red-100">83%</span>
      </p>
      <div className="result__tabs w-3/5 mx-auto">
        <div className="audio w-full ">
          <div className="flex justify-between items-center">
            <span className="audio__title text-1xl">Your Recorded Audio</span>
          </div>

          <div>{/* Wave Audio  */}</div>
          <div>
            <Waveform />
          </div>
          <div className="audio__text">
            <p className="font-bold text-1xl mt-4 mb-2">Pronunciation</p>
            <span className="text-red-100">Coconut</span>
            <span className='text-[#A9A9A9] line-through	'> Coonut</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Index;
