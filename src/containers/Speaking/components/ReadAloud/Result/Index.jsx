import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Tabs } from 'antd';
import Waveform from '../../../../../components/WaveForm/Index';
import speakingActions from 'src/redux/speaking/actions';
import ReactHtmlParser from 'react-html-parser';

const { getResultReadAloud } = speakingActions;

const { TabPane } = Tabs;

function Index() {
  const { yourRecorded, resultReadAloud } = useSelector(
    (state) => state.Speaking
  );
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const [arrStyle, setArrStyle] = useState();

  useEffect(() => {
    if (Object.values(resultReadAloud).length !== 0) {
      setData(resultReadAloud);
      handleCheckResult();
    }
  }, [resultReadAloud]);

  useEffect(() => {
    if (Object.values(data).length !== 0) {
      handleCheckResult();
    }
  }, [data]);

  const handleCheckResult = () => {
    let arrParagraphOriginal =
      data?.paragraphResult?.paragraphOriginal.split(' ');
    let arrStyle1 = arrParagraphOriginal?.map((x) => {
      return `<span class="text-[#357A9B]">${x}</span>`;
    });

    data.listIndexMissed?.map((x) => {
      arrStyle1[x] = `<span class="text-red-100">${arrParagraphOriginal.find(
        (y, idx) => idx === x
      )}</span>`;
    });

    data.datas?.map((x) => {
      let numParAdd = arrStyle1?.length - arrParagraphOriginal.length;
      arrStyle1.splice(
        x.indexKeyCorrectParagraph + numParAdd,
        0,
        `<span class="line-through">${x.listWordIncorrect.join(' ')}</span>`
      );
    });

    setArrStyle(arrStyle1?.join(' '));
  };

  useEffect(() => {
    dispatch(getResultReadAloud(yourRecorded));
  }, []);

  const onChange = (key) => {
    console.log(key);
  };

  return (
    <div className="lm-speak-aloud-result">
      <p className="result__title text-2xl text-center font-bold py-[32px]">
        MY SCORE:{' '}
        <span className="text-red-100">
          {data?.paragraphResult?.percentCorrect}
        </span>
      </p>
      <div className="result__tabs">
        <Tabs defaultActiveKey="1" onChange={onChange}>
          <TabPane tab="Your Response" key="response">
            <div className="flex gap-10">
              <div className="audio w-[50%] max-w-[50%] ">
                <div className="flex justify-between items-center">
                  <span className="audio__title text-1xl">Sample Audio</span>
                  <div className="flex">
                    <div className="male text-[12px]">Male</div>
                    <div className="female text-[12px]">Female</div>
                  </div>
                </div>

                <div>
                  <Waveform />
                </div>

                <div className="audio__text">
                  <p className="font-bold text-1xl mt-4 mb-2">Original Text</p>
                  <p>{data?.paragraphResult?.paragraphOriginal}</p>
                </div>
              </div>

              <div className="audio w-[50%] ">
                <div className="flex justify-between items-center">
                  <span className="audio__title text-1xl">
                    Your Recorded Audio
                  </span>
                </div>

                <div>
                  <Waveform />
                </div>

                <div className="audio__text">
                  <p className="font-bold text-1xl mt-4 mb-2">Pronunciation</p>
                  <p>{ReactHtmlParser(arrStyle)}</p>
                </div>
              </div>
            </div>
          </TabPane>
          <TabPane tab="Sample Solution" key="solution">
            Sample Solution
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
}

export default Index;
