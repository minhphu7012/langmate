import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import RecordedAnswer from 'src/components/RecordedAnswer/Index';
import SpeakVoice from 'src/components/SpeakVoice/Index';
import RecorderToMp3 from 'src/components/RecorderToMp3/index';
import speakingActions from 'src/redux/speaking/actions';

const { getReadAloud, saveParagraph } = speakingActions;

function Index() {
  const { dataReadAloud } = useSelector((state) => state.Speaking);
  const [data, setData] = useState('');
  const dispatch = useDispatch();
  const text = useRef({});

  function setVoice(value) {
    text.current.paragraphYourRecoreded = value;
  }

  useEffect(() => {
    dispatch(getReadAloud());
  }, [dispatch]);

  useEffect(() => {
    if (dataReadAloud?.data?.length > 0) {
      setData(dataReadAloud?.data[0]);
      text.current.id = dataReadAloud?.data[0].id;
    }
  }, [dataReadAloud]);

  useEffect(() => {
    return () => {
      dispatch(
        saveParagraph({
          idQuestion: text?.current.id,
          yourRecorded: text?.current.paragraphYourRecoreded,
          // yourRecorded:
          //   'bold gardens are scientific and structure one institutions established to collect, study, exchange and display plants for research and for the education and enjoyment of the public. There are major botanic gardens in each capital city. Zoological parks and aquariums are primarily engaged in the breeding, preservation and display of native and exotic fun in captivity.',
          timeYourAnswer: 0,
        })
      );
    };
  }, []);

  return (
    <div className="lm-read-aloud mt-[32px]">
      <div className="flex flex-col items-center">
        <p className="font-bold mb-[32px]">
          Look at the text below. In 40 seconds, you must read this text aloud
          as naturally and clearly as possible. You have{' '}
          <span className="text-red-100">40 seconds </span>
          to read aloud.
        </p>
        <RecordedAnswer data={data} title="Recorded Answer" />
        <div className="line my-[32px]"></div>
        <div className="topic">{data.paragraph}</div>
      </div>

      <div className="w-3/5 mx-auto mt-10">
        <SpeakVoice setVoice={setVoice} />
      </div>

      {/* <div>
        <TextToSpeech />
      </div> */}

      {/* <div>
        <RecorderToMp3 />
      </div> */}
    </div>
  );
}

export default Index;
