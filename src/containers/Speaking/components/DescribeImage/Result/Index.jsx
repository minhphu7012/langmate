import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { Tabs } from 'antd';
const { TabPane } = Tabs;
import Waveform from '../../../../../components/WaveForm/Index';

function Index() {
  const onChange = (key) => {
    console.log(key);
  };

  return (
    <div className="lm-speak-aloud-result">
      <p className="result__title text-2xl text-center font-bold py-[32px]">
        MY SCORE: <span className="text-red-100">83%</span>
      </p>
      <div className="result__tabs w-3/5 mx-auto">
        <Tabs defaultActiveKey="1" onChange={onChange}>
          <TabPane tab="Your Response" key="response">
            <div className="flex items-center gap-10">
              <div className="audio w-full ">
                <div className="flex justify-between items-center">
                  <span className="audio__title text-1xl">Sample Audio</span>
                  <div className="flex">
                    <div className="male text-[12px]">Male</div>
                    <div className="female text-[12px]">Female</div>
                  </div>
                </div>

                <div>
                  <Waveform />
                </div>

                <div className="audio__text">
                  <p className="font-bold text-1xl mt-4 mb-2">Original Text</p>
                  <p>
                    For any marketing course␣ that requires the development of a
                    marketing plan, such as Marketing Management, Marketing
                    Strategy and Principles of Marketing. This is the only
                    planning handbook␣ that guides students through step by step
                    creation of a customized marketing plan ␣while offering
                    commercial software to aid in the process. Your recorded
                    audio
                  </p>
                </div>
              </div>
            </div>
          </TabPane>
          <TabPane tab="Sample Solution" key="solution">
            Sample Solution
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
}

export default Index;
