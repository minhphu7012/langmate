import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { Select } from 'antd';
import RecordedAnswer from 'src/components/RecordedAnswer/Index';
import ChartImg from 'public/images/describe-image-chart.png';

function Index() {
  return (
    <div className="lm-read-aloud mt-[32px]">
      <div className="flex flex-col items-center">
        <p className="font-bold mb-[32px]">
          Look at the image below. In 25 seconds, please speak into the
          microphone and describe in detail what the image is showing. You will
          have
          <span className="text-red-100"> 40 seconds </span>
          to give your response.
        </p>
        <div className="flex justify-between items-center w-4/5">
          <Image src={ChartImg} alt="chart" />
          <RecordedAnswer title="Recorded Answer" />
        </div>
      </div>
    </div>
  );
}

export default Index;
