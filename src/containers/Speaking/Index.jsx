import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { Select } from 'antd';
const { Option } = Select;
import RecorderToMp3 from 'src/components/RecorderToMp3/index';

function Index() {
  return (
    <div>

      <div className="mt-5">
        <RecorderToMp3 />
      </div>
    </div>
  );
}

export default Index;
