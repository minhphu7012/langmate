import React from 'react';
import Link from 'next/link';
import LogoFooter from 'public/icons/ic-landing-footer-logo.svg';
import IcScrollDown from 'public/icons/ic-hero-scroll-down.svg';
import IcMessage from 'public/icons/ic-landing-footer-message.svg';
import IcLocation from 'public/icons/ic-landing-footer-location.svg';
import IcPhone from 'public/icons/ic-landing-footer-phone.svg';
import IcFaceBook from 'public/icons/ic-landing-footer-fb.svg';
import IcInstagram from 'public/icons/ic-landing-footer-instagram.svg';
import IcYoutube from 'public/icons/ic-landing-footer-youtube.svg';
import IcPlus from 'public/icons/ic-landing-footer-plus.svg';
import ImgVideoFrame from 'public/images/bg-video-frame.svg';
import ImgFormService from 'public/images/img-landing-form-service.svg';
import IcTick from 'public/icons/ic-testimonials-tick.svg';
import IcPartnersAirbnd from 'public/icons/ic-landing-partners-airbnb.svg';
import IcPartnersAmazon from 'public/icons/ic-landing-partners-amazon.svg';
import IcPartnersFacebook from 'public/icons/ic-landing-partners-facebook.svg';
import IcPartnersGoogle from 'public/icons/ic-landing-partners-google.svg';
import IcPartnersApple from 'public/icons/ic-landing-partners-apple.svg';
import IcPartnersSquare from 'public/icons/ic-landing-partners-square.svg';
import IcBubbleMess from 'public/icons/ic-landing-bubble-mess.svg';
import IcBubbleAvatar from 'public/icons/ic-landing-bubble-avatar.svg';
import IcCard1 from 'public/icons/ic-landing-card-1.svg';
import IcCard2 from 'public/icons/ic-landing-card-2.svg';
import IcCard3 from 'public/icons/ic-landing-card-3.svg';
import Image from 'next/image';
import TrustElementsSlider from 'src/components/LanguageMates/TrustElementsSlider';
import CustomerSlider from 'src/components/LanguageMates/CustomerSlider';
import Header from 'src/components/LanguageMates/Header';

function Index(props) {
  return (
    <div className="wrapper">
      {/* Header */}
      <Header />
      {/* End Header */}
      {/* Hero */}
      <section className="hero">
        <div className="hero__container">
          <div className="hero__title">
            <h2>
              An advanced platform for better
              <span>PTE learning experiences</span>
            </h2>
            <p>
              Many smart features to make your learning easier and more
              effective than other methods.
            </p>
          </div>
          <div className="hero__image flex justify-center">
            <Image src={ImgVideoFrame} alt="Image Video Frame" />
          </div>
          <div className="flex justify-center">
            <button className="hero__scroll-down flex items-center justify-center">
              <Image src={IcScrollDown} alt="Scroll down" />
              <span>Scroll down</span>
            </button>
          </div>
        </div>
      </section>
      {/* End Hero */}
      {/* Trust Elements */}
      <section className="trust-elements">
        <div className="trust-elements__container">
          <TrustElementsSlider />
          <ul className="cards flex justify-between">
            <li className="flex flex-col items-center">
              <Image src={IcCard1} alt="Card" />
              <p>
                Integrate so many smart features to make your learning easier
                and more effective than other methods.
              </p>
            </li>
            <li className="flex flex-col items-center">
              <Image src={IcCard2} alt="Card" />
              <p>Real exam questions are updated regularly.</p>
            </li>
            <li className="flex flex-col items-center">
              <Image src={IcCard3} alt="Card" />
              <p>Real exam questions are updated regularly.</p>
            </li>
          </ul>
        </div>
      </section>
      {/* End Trust Elements */}
      {/* MemberShip */}
      <section className="membership">
        <div className="membership__container">
          <div className="membership__title">
            <h1>MEMBERSHIP</h1>
            <p>Choose your most suitable way of learning</p>
          </div>
          <ul className="membership__list flex justify-between">
            <li className="membership__item flex flex-col justify-between">
              <div className="item__header item__header--free flex items-center justify-between">
                <h4>Free trail</h4>
                <span>Free</span>
              </div>
              <div className="item__content flex-grow flex flex-col justify-between">
                <ul>
                  <li>Access to basic features only.​</li>
                  <li>The number of practice questions is limited. ​</li>
                  <li>
                    ​ Able to check results and scoring after completing a task.
                  </li>
                </ul>
                <p>
                  Disclaimer: ​ (This package is designed to help learners get
                  to know the real test formats only. For better result, please
                  subscribe Basic or Advanced packages)
                </p>
              </div>
              <div className="item__register">
                <button>Register Now​</button>
              </div>
            </li>
            <li className="membership__item flex flex-col justify-between">
              <div className="item__header item__header--basic flex items-center justify-between">
                <h4>Basic</h4>
                <span>30 AUD/ month</span>
              </div>
              <div className="item__content flex-grow flex flex-col justify-between">
                <ul>
                  <li>
                    Acess to all practice questions, including the latest real
                    exam questions. ​ ​ ​ ​
                  </li>
                  <li>
                    Able to check results and scoring after completing a task.​
                    ​
                  </li>
                  <li>
                    ​ Solution for each practice question: this part provides
                    detailed explaination of why the result is right or wrong,
                    and tips/strategies for learners’ better self-improvement.
                  </li>
                  <li>​Bookmark and search questions.</li>
                  <li>Dictionary feature for Reading part.</li>
                </ul>
              </div>
              <div className="item__register">
                <button>Register Now​</button>
              </div>
            </li>
            <li className="membership__item flex flex-col justify-between">
              <div className="item__header item__header--premium flex items-center justify-between">
                <h4>Premium</h4>
                <span>45 AUD/ month</span>
              </div>
              <div className="item__content flex-grow flex flex-col justify-between">
                <ul>
                  <li>
                    Acess to all practice questions, including the latest real
                    exam questions. ​ ​ ​ ​
                  </li>
                  <li>
                    Able to check results and scoring after completing a task.​
                    ​
                  </li>
                  <li>
                    ​ Solution for each practice question: this part provides
                    detailed explaination of why the result is right or wrong,
                    and tips/strategies for learners’ better self-improvement.
                  </li>
                  <li>Bookmark and search questions.​</li>
                  <li>​ Dictionary feature for all practice parts.</li>
                  <li>​ Send questions to trainers and get feedback.</li>
                </ul>
              </div>
              <div className="item__register">
                <button>Register Now​</button>
              </div>
            </li>
          </ul>
        </div>
      </section>
      {/* End MemberShip */}
      {/* Testimonials */}
      <section className="testimonials">
        <div className="testimonials__container">
          <div className="service">
            <div className="service__title">
              <h1>PTE Mock Test</h1>
              <p>
                Want to quickly assess your PTE score?
                <br />
                Register a FREE MOCK TEST with PTE Mates now!​
              </p>
            </div>
            <div className="service__content flex">
              <div className="content__left">
                <ul>
                  <li className="flex items-center">
                    <Image src={IcTick} alt="tick" />
                    <span>Real exam questions</span>
                  </li>
                  <li className="flex items-center">
                    <Image src={IcTick} alt="tick" />
                    <span>Real exam formats</span>
                  </li>
                  <li className="flex items-center">
                    <Image src={IcTick} alt="tick" />
                    <span>Real experience</span>
                  </li>
                  <li className="flex items-center">
                    <Image src={IcTick} alt="tick" />
                    <span>Get immediate scoring</span>
                  </li>
                </ul>
                <h3>SUPER EASY!​</h3>
              </div>
              <div className="content__right">
                <Image src={ImgFormService} alt="Form Service" />
                <form action="" className="form">
                  <div className="form__row flex items-center justify-between">
                    <div className="form__col">
                      <input type="text" placeholder="Name" />
                    </div>
                    <div className="form__col">
                      <input type="text" placeholder="Email" />
                    </div>
                  </div>
                  <div className="form__row flex items-center justify-between">
                    <div className="form__col">
                      <input type="text" placeholder="Phone number" />
                    </div>
                    <div className="form__col flex items-center justify-between">
                      <input
                        type="text"
                        placeholder="Courses you Interested in"
                      />
                      <Image src={IcPlus} alt="Plus" />
                    </div>
                  </div>
                  <button className="form__submit">
                    Sign-up for FREE MOCK TEST ​
                  </button>
                </form>
              </div>
            </div>
          </div>
          <div className="customer">
            <h1>What’s our customers say</h1>
            <CustomerSlider />
          </div>
          <div className="partners">
            <h1>Our Partners</h1>
            <ul className="flex justify-around items-center">
              <li className="flex items-center justify-center">
                <Image src={IcPartnersSquare} alt="Square" />
              </li>
              <li className="flex items-center justify-center">
                <Image src={IcPartnersApple} alt="Apple" />
              </li>
              <li className="flex items-center justify-center">
                <Image src={IcPartnersFacebook} alt="Facebook" />
              </li>
              <li className="flex items-center justify-center">
                <Image src={IcPartnersAirbnd} alt="Airbnb" />
              </li>
              <li className="flex items-center justify-center">
                <Image src={IcPartnersGoogle} alt="Google" />
              </li>
              <li className="flex items-center justify-center">
                <Image src={IcPartnersAmazon} alt="Amazon" />
              </li>
            </ul>
          </div>
        </div>
      </section>
      {/* End Testimonials */}
      {/* Bubble */}
      <div className="bubble flex flex-col">
        <Image src={IcBubbleMess} alt="Mess" />
        <Image src={IcBubbleAvatar} alt="Avatar" />
      </div>
      {/* End Bubble */}
      {/* Footer */}
      <footer className="footer">
        <div className="footer__container">
          <div className="footer__center flex">
            <div className="center__left flex flex-col">
              <div>
                <Image src={LogoFooter} alt="Logo" />
              </div>
              <ul className="flex-grow flex flex-col justify-between">
                <li className="flex items-center">
                  <Image src={IcMessage} alt="Email" />
                  <span>hello@languagemates.com</span>
                </li>
                <li className="flex items-center">
                  <Image src={IcPhone} alt="Email" />
                  <span>+91 98765 43210</span>
                </li>
                <li className="flex items-center">
                  <Image src={IcLocation} alt="Email" />
                  <span>326 Nguyễn Thị Minh Khai, Quận 3, Tp.HCM</span>
                </li>
                <li className="flex items-center">
                  <Link href="/">
                    <a>
                      <Image src={IcInstagram} alt="Email" />
                    </a>
                  </Link>
                  <Link href="/">
                    <a>
                      <Image src={IcFaceBook} alt="FaceBook" />
                    </a>
                  </Link>
                  <Link href="/">
                    <a>
                      <Image src={IcYoutube} alt="Youtube" />
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
            <div className="center__right">
              <form className="footer__form">
                <div className="form__row flex items-center justify-between">
                  <div className="form__col">
                    <input type="text" placeholder="Name" />
                  </div>
                  <div className="form__col">
                    <input type="text" placeholder="Email" />
                  </div>
                </div>
                <div className="form__row flex items-center justify-between">
                  <div className="form__col">
                    <input type="text" placeholder="Phone number" />
                  </div>
                  <div className="form__col flex items-center justify-between">
                    <input
                      type="text"
                      placeholder="Courses you Interested in"
                    />
                    <Image src={IcPlus} alt="Plus" />
                  </div>
                </div>
                <div className="form__row form__row--message flex flex-col">
                  <label htmlFor="">Message</label>
                  <input type="text" />
                </div>
                <button className="form__submit">Send</button>
              </form>
            </div>
          </div>
          <div className="footer__bottom flex items-center justify-between">
            <p>© 2022 LanguageMates. All rights reserved</p>
            <ul className="flex items-center">
              <li>Terms & Conditions</li>
              <li>Privacy Policy</li>
              <li>Sitemap</li>
              <li>Disclaimer</li>
            </ul>
          </div>
        </div>
      </footer>
      {/* End Footer */}
    </div>
  );
}

export default Index;
