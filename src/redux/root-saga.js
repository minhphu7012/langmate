import { all } from 'redux-saga/effects';
import speaking from 'src/redux/speaking/saga';

export default function* rootSaga(getState) {
  yield all([speaking()]);
}
