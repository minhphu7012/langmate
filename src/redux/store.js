import { createWrapper, HYDRATE } from 'next-redux-wrapper';
import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './root-reducer';
import rootSaga from './root-saga';

// // create the saga middleware
// const sagaMiddleware = createSagaMiddleware()
// // mount it on the Store
// const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
// // then run the saga
// sagaMiddleware.run(rootSaga)

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

// const mainReducer = (state = {}, action) => action.type === HYDRATE ? action.payload : rootReducer(state, action);
const mainReducer = (state = {}, action) => {
  switch (action.type) {
    case HYDRATE:
      const res = { ...state, ...action.payload };
      return res;
    default:
      return rootReducer(state, action);
  }
};

export const makeStore = (context, initialState) => {
  // 1: Create the middleware
  const sagaMiddleware = createSagaMiddleware();

  // 2: Add an extra parameter for applying middleware:
  // const store = createStore(mainReducer, initialState, applyMiddleware(sagaMiddleware));
  const store = createStore(
    mainReducer,
    initialState,
    bindMiddleware([sagaMiddleware])
  );

  // 3: Run your sagas on server
  store.sagaTask = sagaMiddleware.run(rootSaga);

  // 4: now return the store:
  return store;
};

export const wrapper = createWrapper(makeStore, { debug: false });
