import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import actions from './actions';
import * as speakingServices from 'src/services/speaking';

export function* getReadAloud() {
  yield takeEvery(actions.GET_READ_ALOUD, function* ({ payload }) {
    const { model } = payload;

    try {
      var response = yield call(speakingServices.getReadAloud, model);
      if (response.data.success === true) {
        var data = response.data.data;
        yield put({
          type: actions.GET_READ_ALOUD_SUCCESS,
          payload: { data },
        });
      }
    } catch (e) {
      yield put({ type: actions.LANDINGPAGE_LOADING_FALSE });
    }
  });
}

export function* getResultReadAloud() {
  yield takeEvery(actions.GET_RESULT_READ_ALOUD, function* ({ payload }) {
    const { body } = payload;

    try {
      var response = yield call(speakingServices.getResultReadAloud, body);
      if (response.data.success === true) {
        var data = response.data.data;
        yield put({
          type: actions.GET_RESULT_READ_ALOUD_SUCCESS,
          payload: { data },
        });
      }
    } catch (e) {
      yield put({ type: actions.LANDINGPAGE_LOADING_FALSE });
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getReadAloud), fork(getResultReadAloud)]);
}
