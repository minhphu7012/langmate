import actions from './actions';

const initState = {
  dataReadAloud: [],
  resultReadAloud: {},
  listQuestion: [],
  yourRecorded: {},
};

export default function reducer(state = initState, action) {
  switch (action.type) {
    case actions.GET_READ_ALOUD:
      return { ...state };

    case actions.GET_READ_ALOUD_SUCCESS:
      const { data } = action.payload;
      return { ...state, dataReadAloud: data, listQuestion: data.listQuestion };

    case actions.SAVE_PARAGRAPH:
      return { ...state };

    case actions.SAVE_PARAGRAPH_SUCCESS:
      return { ...state, yourRecorded: action.payload.body };

    case actions.GET_RESULT_READ_ALOUD:
      return { ...state };

    case actions.GET_RESULT_READ_ALOUD_SUCCESS:
      // const { data } = action.payload;
      return { ...state, resultReadAloud: action.payload.data };

    case actions.LOADING_FALSE: {
      return { ...state, loading: false };
    }

    default:
      return state;
  }
}
