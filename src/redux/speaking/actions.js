const actions = {
  GET_READ_ALOUD: 'GET_READ_ALOUD',
  GET_READ_ALOUD_SUCCESS: 'GET_READ_ALOUD_SUCCESS',

  SAVE_PARAGRAPH: 'SAVE_PARAGRAPH',
  SAVE_PARAGRAPH_SUCCESS: 'SAVE_PARAGRAPH_SUCCESS',

  GET_RESULT_READ_ALOUD: 'GET_RESULT_READ_ALOUD',
  GET_RESULT_READ_ALOUD_SUCCESS: 'GET_RESULT_READ_ALOUD_SUCCESS',

  LOADING_FALSE: 'LOADING_FALSE',
  //function
  getReadAloud: (model) => ({
    type: actions.GET_READ_ALOUD,
    payload: { model },
  }),

  saveParagraph: (body) => ({
    type: actions.SAVE_PARAGRAPH_SUCCESS,
    payload: { body },
  }),

  getResultReadAloud: (body) => ({
    type: actions.GET_RESULT_READ_ALOUD,
    payload: { body },
  }),
};

export default actions;
