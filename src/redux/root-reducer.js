import { combineReducers } from 'redux';
import Speaking from 'src/redux/speaking/reducer';

export default combineReducers({
  Speaking,
});
