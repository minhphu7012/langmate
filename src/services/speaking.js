import { api } from 'src/utils/api/axios.configs';
import { ApiRouters } from 'src/utils/api/apiRouters';

export const getReadAloud = (model) => {
  var url = `${ApiRouters.SPEAKING}/ReadAloud`;
  return api.get(url, { params: model });
};

export const getResultReadAloud = (body) => {
  var url = `${ApiRouters.SPEAKING}/LastSolution`;
  return api.post(url, body);
};
